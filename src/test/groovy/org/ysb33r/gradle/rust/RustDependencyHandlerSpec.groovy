//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import org.gradle.api.Action
import org.ysb33r.gradle.rust.dependencies.RustConfigurableDependency
import org.ysb33r.gradle.rust.dependencies.RustDependencyConfiguration
import org.ysb33r.gradle.rust.dependencies.RustDependencyFormatException
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler
import org.ysb33r.gradle.rust.internal.dependencies.ConfigurableDependency
import org.ysb33r.gradle.rust.internal.dependencies.DependencyHandler
import org.ysb33r.gradle.rust.internal.dependencies.NameVersionDependency
import spock.lang.Specification

class RustDependencyHandlerSpec extends Specification {

    RustDependencyHandler rust = new DependencyHandler()

    def 'Can define dependencies by string'() {
        when:
        // tag::simple-dep[]
        rust {
            compile 'winhttp:0.4.0'
        }
        // tag::simple-dep[]

        then:
        rust.compile.dependencies.size() == 1

        when:
        def dep = rust.compile.dependencies[0]

        then:
        dep instanceof NameVersionDependency
        dep.name == 'winhttp'
        dep.version == '0.4.0'
        dep.toString() == "winhttp = \"0.4.0\""
    }

    def 'Can define mutiple dependencies by string'() {
        when:
        // tag::simple-dep[]
        rust {
            test 'winhttp:0.4.0', 'foo:1.2.3'
        }
        // tag::simple-dep[]

        then:
        rust.test.dependencies.size() == 2

        when:
        def dep0 = rust.test.dependencies[0]
        def dep1 = rust.test.dependencies[1]

        then:
        dep0 instanceof NameVersionDependency
        dep0.name == 'winhttp'
        dep0.version == '0.4.0'
        dep0.toString() == "winhttp = \"0.4.0\""
        dep1 instanceof NameVersionDependency
        dep1.name == 'foo'
        dep1.version == '1.2.3'
        dep1.toString() == "foo = \"1.2.3\""
    }

    def 'Can define dependencies by closure'() {
        when:
        rust {
            compile {
                name 'winhttp'
                version '0.4.0'
            }

            test {
                name 'uuid_1'
                git 'https://github.com/foo/bar'
            }

            build {
                name 'uuid_2'
                git 'https://github.com/foo/bar', 'my-branch'
            }

            compile {
                name 'something'
                path 'path/to/something'
            }

            test {
                name 'something_else'
                version '1.0.0'
                path 'path/to/something_else'
            }
        }

        def dep0 = rust.compile.dependencies[0]
        def dep1 = rust.test.dependencies[0]
        def dep2 = rust.build.dependencies[0]
        def dep3 = rust.compile.dependencies[1]
        def dep4 = rust.test.dependencies[1]

        then:
        dep0 instanceof ConfigurableDependency
        dep0.toString() == "winhttp = \"0.4.0\""

        dep1 instanceof ConfigurableDependency
        dep1.toString() == "uuid_1 = { git = \"https://github.com/foo/bar\" }"

        dep2 instanceof ConfigurableDependency
        dep2.toString() == "uuid_2 = { git = \"https://github.com/foo/bar\", branch = \"my-branch\" }"

        dep3 instanceof ConfigurableDependency
        dep3.toString() == "something = { path = \"path/to/something\" }"

        dep4 instanceof ConfigurableDependency
        dep4.toString() == "something_else = { path = \"path/to/something_else\", version = \"1.0.0\" }"

        rust.compile.targetDependencies.size() == 0
    }

    def 'Can define dependencies by action'() {
        setup:
        def item0 = new Action<RustConfigurableDependency>() {
            @Override
            void execute(RustConfigurableDependency o) {
                o.name 'foobar'
                o.version '1.2.3'
            }
        }

        when:
        rust {
            compile item0
        }

        def dep0 = rust.compile.dependencies[0]

        then:
        dep0 instanceof ConfigurableDependency
        dep0.toString() == "foobar = \"1.2.3\""
    }

    def 'Cannot set path after git'() {
        when:
        rust {
            compile {
                name 'foo'
                git 'http://a/b/c'
                path '/a/b/c'
            }
        }

        then:
        thrown(RustDependencyFormatException)
    }

    def 'Cannot set git after path'() {
        when:
        rust {
            compile {
                name 'foo'
                path '/a/b/c'
                git 'http://a/b/c'
            }
        }

        then:
        thrown(RustDependencyFormatException)
    }

    def 'Add patches to dependencies'() {
        when:
        rust {
            patch 'foo', '../path/to/foo'
            patch 'http://foo/bar'.toURI(), 'bar', '../path/to/bar'
            patch {
                name 'foo2'
                path '../path/to/foo2'
            }
            patch ('http://foo/bar'.toURI()) {
                name 'foo3'
                path '../path/to/foo3'
            }
            patch {
                name 'foo4'
                git 'https://gitlab.com/foo/bar'
            }
        }

        def patches = rust.patches
        def map = rust.patchesAsMap

        then:
        patches.size() == 5

        map['crates-io'].size() == 3
        map['crates-io'][0].toString() == 'foo = { path = "../path/to/foo" }'
        map['crates-io'][1].toString() == 'foo2 = { path = "../path/to/foo2" }'
        map['crates-io'][2].toString() == 'foo4 = { git = "https://gitlab.com/foo/bar" }'

        map['http://foo/bar'].size() == 2
        map['http://foo/bar'][0].toString() == 'bar = { path = "../path/to/bar" }'
        map['http://foo/bar'][1].toString() == 'foo3 = { path = "../path/to/foo3" }'
    }

    def 'Can add target overrides'() {
        when:
        rust {
            compile {
                target 'cfg(windows)'
                name 'winhttp'
                version '1.2.3'
            }
        }

        def deps = rust.compile.targetDependencies

        then:
        rust.compile.dependencies.size() == 0
        deps.size() == 1
        deps['cfg(windows)'].size() == 1
        !deps['cfg(windows)'][0].hasFeatures
        deps['cfg(foo)'] == null
        deps['cfg(windows)'][0].toString() == 'winhttp = "1.2.3"'
    }

    def 'Can specify features'() {
        when:
        rust {
            compile {
                name 'winhttp'
                version '1.2.3'
                withDefaultFeatures false
                features 'a', 'b'
            }
        }

        def deps = rust.compile.dependencies
        def features = deps[0].features

        then:
        deps[0].hasFeatures
        features.size() == 2
        features.contains 'a'
        features.contains 'b'
    }
}