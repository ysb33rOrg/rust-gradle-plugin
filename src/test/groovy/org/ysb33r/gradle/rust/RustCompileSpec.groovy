//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.rust.helpers.RustRunnerSpecification
import org.ysb33r.gradle.rust.plugins.RustBasePlugin
import spock.lang.Issue
import spock.lang.PendingFeature

class RustCompileSpec extends RustRunnerSpecification {

    void 'Compile a Rust application'() {

        given:
        GradleRunner runner = runnerFromProject('helloWorld').withArguments('assemble', '-s').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir,"build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/target/debug/helloWorld").exists()
    }

    void 'Compile a Rust library'() {

        given:
        GradleRunner runner = runnerFromProject('helloWorldWithDependencies').withArguments('assemble', '-s').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir,"build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/target/debug/libhelloWorldWithDependencies.rlib").exists()
    }

    void 'Compile Rust test code'() {
        given:
        GradleRunner runner = runnerFromProject('testWithRust').withArguments('compileTestRust', '-s').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir,"build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/target/debug/").listFiles().find {
            it.name.startsWith('test-') && it.name.endsWith('.d')
        }
    }

    @Issue('https://gitlab.com/ysb33rOrg/rust-gradle-plugin/issues/15')
    @PendingFeature
    void 'Compile Rust library using Cargo.toml as spec, but with Gradle layout'() {
        given:
        GradleRunner runner = runnerFromProject('helloWorldWithCargoToml').withArguments('assemble').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir,"build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/target/debug/libhelloWorldWithCargoToml.rlib").exists()
    }

    @Issue('https://gitlab.com/ysb33rOrg/rust-gradle-plugin/issues/15')
    @PendingFeature
    void 'Compile Rust application using Cargo.toml as spec, but with Gradle layout'() {
        given:
        GradleRunner runner = runnerFromProject('helloWorldApplicationWithCargoToml').withArguments('assemble','-i').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir,"build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/target/debug/helloWorldApplicationWithCargoToml").exists()

    }

}