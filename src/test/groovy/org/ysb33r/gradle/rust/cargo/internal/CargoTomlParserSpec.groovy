//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo.internal

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.rust.cargo.CargoManifest
import org.ysb33r.gradle.rust.dependencies.RustDependency
import org.ysb33r.gradle.rust.internal.dependencies.DependencyHandler
import spock.lang.Specification

class CargoTomlParserSpec extends Specification {
    static final String TEST_RESOURCES = './src/test/resources'

    Project project = ProjectBuilder.builder().build()

    def 'Parse a Cargo.toml file'() {
        given:
        File cargoToml = new File(TEST_RESOURCES, 'Cargo.toml')
        CargoPackage cp = new CargoPackage(project)
        DependencyHandler dh = new DependencyHandler(project)
        CargoTomlParser parser = new CargoTomlParser(project, cp, dh)

        when:
        parser.parseFile(cargoToml)
        RustDependency glib = dh.compile.dependencies.find { it.name == 'glib' }
        RustDependency gstreamer = dh.compile.dependencies.find { it.name == 'gstreamer-mpegts' }
        RustDependency gtk = dh.compile.dependencies.find { it.name == 'gtk' }

        then:
        cp.version == '3.0.0'
        cp.name == 'me-tv'
        cp.authors == ['Russel Winder <russel@winder.org.uk>']

        and:
        dh.compile.dependencies.find { it.name == 'gdk' && it.version == '*' }
        dh.test.dependencies.find { it.name == 'quickcheck' && it.version == '*' }

        and:
        glib.hasFeatures
        glib.features == ['v2_40']
        glib.version == '*'

        and:
        gstreamer.toString() == 'gstreamer-mpegts = { path = "../../Masters/GStreamer-MPEG-TS-Rust" }'

        and:
        gtk.hasFeatures
        gtk.version == '1.0'
        gtk.features == ['v3_10']

        and:
        dh.build.targetDependencies['cfg(foo)'].find { it.name == 'xdg' && it.version == '*' }
        dh.compile.targetDependencies['i686-unknown-linux-gnu'].find { it.name == 'openssl' && it.version == '1.0.1' }

        and:
        dh.patchesAsMap[DependencyHandler.CRATES_IO_NAME].find {
            it.toString() == 'uuid = { git = "https://github.com/rust-lang-nursery/uuid" }'
        }
        dh.patchesAsMap['https://github.com/your/repository'].find {
            it.toString() == 'my-library = { path = "../my-library/path" }'
        }
    }
}