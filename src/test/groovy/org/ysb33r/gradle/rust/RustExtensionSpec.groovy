//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.rust.helpers.RustRunnerSpecification
import org.ysb33r.gradle.rust.internal.RustInstaller
import spock.lang.Specification

class RustExtensionSpec extends Specification {

    final static File RUST_REPO = RustRunnerSpecification.RUST_REPO_FILE
    final static String CARGO_NAME = RustInstaller.OS.isWindows() ? 'cargo.exe' : 'cargo'
    final static String RUSTC_NAME = RustInstaller.OS.isWindows() ? 'rustc.exe' : 'rustc'

    Project project = ProjectBuilder.builder().build()
    RustExtension rustExtension

    void setup() {
        rustExtension = new RustExtension(project)
        RustInstaller.baseURI = RUST_REPO.toURI().toString()
    }

    void 'Can configure system system path to find rustc'() {
        when:
        rustExtension.with {
            executable SEARCH_PATH
        }

        then:
        rustExtension.SEARCH_PATH.search == 'rustc'
    }

    void 'Can resolve paths to cargo and rustc'() {
        when:
        File cargo = rustExtension.resolveCargoPath()

        then:
        cargo.name == CARGO_NAME
        cargo.exists()

        when:
        File rustc = rustExtension.resolveRustcPath()

        then:
        rustc.name == RUSTC_NAME
        rustc.exists()
    }

    void 'Default search path for ABI tools is system search path'() {
        given:
        FileCollection systemPath = project.files(RustInstaller.OS.path)

        when:
        FileCollection abiSearchPath = rustExtension.getAbiToolsSearchPath()

        then:
        abiSearchPath.asPath == systemPath.asPath
    }

    void 'System search path can be explicitly configured'() {
        given:
        String osPrefix = RustInstaller.OS.isWindows() ? 'C:' : ''
        String pathSep = RustInstaller.OS.pathSeparator

        when:
        rustExtension.with {
            abiToolsSearchPath = [ "${osPrefix}/bin" ]
        }

        then:
        rustExtension.abiToolsSearchPath.asPath == "${osPrefix}/bin"

        when:
        rustExtension.with {
            abiToolsSearchPath "${osPrefix}/usr/bin"
        }

        then:
        rustExtension.abiToolsSearchPath.asPath == "${osPrefix}/bin${pathSep}${osPrefix}/usr/bin"

    }
}