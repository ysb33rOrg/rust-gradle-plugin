//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.rust.helpers.RustRunnerSpecification
import org.ysb33r.gradle.rust.plugins.RustBasePlugin
import spock.lang.Specification

class ProcessRustSourcesSpec extends RustRunnerSpecification {

    void 'Validate Cargo is written correctly when read from existing template'() {
        given:
        GradleRunner runner = runnerFromProject('helloWorldWithCargoToml').withArguments('processRustSource').withDebug(true)

        when:
        BuildResult result = runner.build()
        File cargoToml = new File(testProjectDir, "build/${RustBasePlugin.RUST_PROJECT_WORKDIR}/Cargo.toml")
        String tomlContents = cargoToml.text

        then:
        tomlContents.contains( 'version = "0.0.2"')
        tomlContents.contains( 'name = "helloWorldWithCargoToml"')
        tomlContents.contains( 'authors = ["ysb33r"]')
    }

}