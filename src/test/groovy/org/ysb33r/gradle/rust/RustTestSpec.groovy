//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.rust.helpers.RustRunnerSpecification

class RustTestSpec extends RustRunnerSpecification {

    void 'Test Rust code'() {
        given:
        GradleRunner runner = runnerFromProject('testWithRust').withArguments('testRust', '-i').withDebug(true)

        when:
        BuildResult result = runner.build()

        then:
        new File(testProjectDir, "build/tmp/cargo/testRust/output.txt").text
            .contains 'test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out'
        new File(testProjectDir, "build/reports/rust/testRust/result.txt").exists()
        result.output.contains('tests completed')
    }

    void 'Failed test will cause build to fail, but still create report'() {
        given:
        GradleRunner runner = runnerFromProject('failedTestWithRust').withArguments('testRust', '-i').withDebug(true)

        when:
        BuildResult result = runner.buildAndFail()

        then:
        new File(testProjectDir, "build/tmp/cargo/testRust/output.txt").text
            .contains 'test result: FAILED. 0 passed; 1 failed; 0 ignored; 0 measured; 0 filtered out'
        new File(testProjectDir, "build/reports/rust/testRust/result.txt").exists()
        result.output.contains('tests completed')
    }

}