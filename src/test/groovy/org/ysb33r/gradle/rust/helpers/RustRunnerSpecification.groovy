//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.helpers

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import java.nio.file.Path

class RustRunnerSpecification extends Specification {
    static final File RUST_PROJECTS = new File(System.getProperty('RUST_PROJECTS') ?: './src/gradleTest').absoluteFile
    static final File PLUGIN_METADATA_FILE = new File(System.getProperty('PLUGIN_METADATA_FILE') ?: './build/test/manifest/plugin-classpath.txt')

    static final String RUST_REPO_URI_STRING = System.getProperty('org.ysb33r.gradle.rust.uri')
    static final URI RUST_REPO_URI = RUST_REPO_URI_STRING ? new URI(RUST_REPO_URI_STRING) : new File('./build/rust-binaries').absoluteFile.toURI()
    static final File RUST_REPO_FILE = RUST_REPO_URI_STRING ? new File(RUST_REPO_URI) : new File( './build/rust-binaries').absoluteFile

    @Rule
    final TemporaryFolder testProjectEnv = new TemporaryFolder()

    File testProjectDir

    void setup() {
        testProjectDir = new File(testProjectEnv.root, 'test_project')
        testProjectDir.deleteDir()
    }

    void cleanup() {
        if(System.getProperty('COPY_TEMPORARY_TEST_DATA_DIR')) {
            File dest = new File(System.getProperty('COPY_TEMPORARY_TEST_DATA_DIR'),testProjectDir.name)
            dest.mkdirs()
            def ant = new AntBuilder()
            ant.sequential {
                copy(todir: dest) {
                    fileset(dir: testProjectDir) {
                        include(name: "**/*.*")
                    }
                }
            }
        }
    }

    void copyProject(final String name) {
        def ant = new AntBuilder()
        ant.sequential {
            copy(todir: testProjectDir) {
                fileset(dir: new File(RUST_PROJECTS, name)) {
                    include(name: "**/*.*")
                }
            }
        }

        File settings = new File(testProjectDir,'settings.gradle')
        if(!settings.exists()) {
            settings.text = ''
        }

//        File build = new File( testProjectDir, 'build.gradle')
//        build.text = build.text.replaceAll()
    }

    GradleRunner createRunner() {
        GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withPluginClasspath(readMetadataFile())
            .forwardOutput()
            .withArguments("-Dorg.ysb33r.gradle.rust.uri=${RUST_REPO_URI}")
    }

    GradleRunner runnerFromProject(final String name) {
        copyProject(name)
        createRunner()
    }

    List<File> readMetadataFile() {
        PLUGIN_METADATA_FILE.readLines().collect {
            new File(it)
        }
    }

}