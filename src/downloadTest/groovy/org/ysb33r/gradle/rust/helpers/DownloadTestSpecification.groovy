//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.helpers

import org.ysb33r.gradle.rust.cargo.CargoExtension
import org.ysb33r.grolifant.api.OperatingSystem
import spock.lang.Specification


class DownloadTestSpecification extends Specification {

    static final String RUST_VERSION = System.getProperty('RUST_VERSION') ?: CargoExtension.CARGO_DEFAULT
    static final File RUST_CACHE_DIR = new File( System.getProperty('RUST_CACHE_DIR') ?: './build/rust-binaries').absoluteFile
    static final File RESOURCES_DIR = new File (System.getProperty('RESOURCES_DIR') ?: './src/downloadTest/resources')

    static final OperatingSystem OS = OperatingSystem.current()
    static final boolean SKIP_TESTS = !(OS.isMacOsX() || OS.isLinux() || OS.isWindows() || OS.isFreeBSD())

}