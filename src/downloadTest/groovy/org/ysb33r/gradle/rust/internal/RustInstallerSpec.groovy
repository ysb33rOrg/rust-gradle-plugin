//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal

import org.gradle.api.Project
import org.gradle.process.ExecResult
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.rust.helpers.DownloadTestSpecification
import spock.lang.IgnoreIf

import java.nio.file.Files


class RustInstallerSpec extends DownloadTestSpecification {

    Project project = ProjectBuilder.builder().build()

    @IgnoreIf({ DownloadTestSpecification.SKIP_TESTS })
    def "Download a Rust distribution" () {
        given: "A requirement to download Rust #RUST_VERSION"
        RustInstaller dwn = new RustInstaller(project, DownloadTestSpecification.RUST_VERSION)
        dwn.downloadRoot = new File(project.buildDir,'download')
        dwn.baseURI = DownloadTestSpecification.RUST_CACHE_DIR.toURI()

        when: "The distribution root is requested"
        File gotIt = dwn.distributionRoot

        String binaryName = DownloadTestSpecification.OS.windows ? 'cargo.exe' : 'cargo'

        then: "The distribution is downloaded, unpacked and installed"
        gotIt != null
        dwn.cargoExecutablePath != null
        dwn.rustcExecutablePath != null

        and: 'The executable has executable rights'
        Files.isExecutable(gotIt.toPath())

        when: "The executable is run to display the help page"
        OutputStream output = new ByteArrayOutputStream()
        ExecResult result = project.exec {
            executable dwn.cargoExecutablePath
            args '--help'
            errorOutput output
        }

        then: "No runtime error is expected"
        result.assertNormalExitValue()

        and: "The expected help information is displayed"
        output.toString().contains('''Some common cargo commands are (see all commands with --list):
    build       Compile the current project
    check       Analyze the current project and report errors, but don't build object files
    clean       Remove the target directory
    doc         Build this project's and its dependencies' documentation
    new         Create a new cargo project
    init        Create a new cargo project in an existing directory
    run         Build and execute src/main.rs
    test        Run the tests
    bench       Run the benchmarks
    update      Update dependencies listed in Cargo.lock
    search      Search registry for crates
    publish     Package and upload this project to the registry
    install     Install a Rust binary
    uninstall   Uninstall a Rust binary''')
    }
}