// Taken from https://doc.rust-lang.org/book/second-edition/ch11-01-writing-tests.html

#[cfg(test)]
mod tests {
    #[test]
    fn exploration() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn exploration2() {
        assert_eq!(2 + 3, 5);
    }

}

