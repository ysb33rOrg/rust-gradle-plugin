// Taken from https://doc.rust-lang.org/book/second-edition/ch11-01-writing-tests.html

#[cfg(test)]
mod tests {
    #[test]
    fn two_plus_two() {
        assert_eq!(2 + 2, 5);
    }

}

