//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.tasks

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.tasks.OutputFile
import org.ysb33r.gradle.rust.tasks.internal.AbstractRustLifecycleTask

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @since 0.1
 */
@CompileStatic
class RustTestTask extends AbstractRustLifecycleTask {

    /** The reports produced by this {@code RustTestTask}.
     *
     * @return File that contains the report generated during the Rust test run.
     */
    @OutputFile
    File getRustTextReport() {
        new File("${project.buildDir}/reports/rust/${name}/result.txt")
    }

    @Override
    protected String getCargoCommand() {
        'test'
    }

    @Override
    protected Iterable<String> getCargoCommandOptions() {
        ['--', '--nocapture']
    }

    /** Post processes the test result.
     *
     * Analysis the test result and creates a log message.
     * Creates a test report file.
     */
    @Override
    protected String postProcess(int exitValue, File output, File error) {

        final String msg = ''

        if (output.exists()) {
            File report = getRustTextReport()
            report.parentFile.mkdirs()
            report.text = output.text

            int numTests = 0
            int passed = 0
            int failed = 0
            int ignored = 0
            int measured = 0
            int filtered = 0
            output.eachLine { String line ->
                Matcher data = line =~ TEST_INDICATORS
                if (data.matches()) {
                    passed += matchAsInteger(data, 1)
                    failed += matchAsInteger(data, 2)
                    ignored += matchAsInteger(data, 3)
                    measured += matchAsInteger(data, 4)
                    filtered += matchAsInteger(data, 5)
                } else if (line.startsWith('running ')) {
                    numTests += line.replaceAll('running ', '').replaceAll(~/\s+tests?/, '').toInteger()
                }
            }

            if (numTests) {
                msg = "${numTests} tests completed, ${passed} passed; ${failed} failed; ${ignored} ignored; ${measured} measured; ${filtered} filtered out."
                if (exitValue) {
                    msg = """${msg}

Test report can be found at ${report.toURI()}.
"""
                }
            }
        } else {
            logger.error "Test task ${name} has produced no output."
        }

        msg
    }

    @CompileDynamic
    private int matchAsInteger(Matcher matcher, int index) {
        try {
            matcher[0][index].toInteger()
        } catch (Exception e) {
            logger.error "Could not interpret index ${index} from ${matcher[0]}"
            0
        }
    }

    /** Matches {@code test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out}.
     *
     */
    static private
    final Pattern TEST_INDICATORS = ~/^\w+\s+result: \w+\. (\d+) passed; (\d+) failed; (\d+) ignored; (\d+) measured; (\d+).+/

}
