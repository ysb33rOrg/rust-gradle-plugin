//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.tasks

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.apache.tools.ant.taskdefs.ManifestTask
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecResult
import org.ysb33r.gradle.rust.internal.RustConventions
import org.ysb33r.gradle.rust.internal.RustInstaller
import org.ysb33r.gradle.rust.plugins.internal.AbstractRustPlugin
import org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile

/** Ability to execute a compiled Rust application.
 *
 * @since 0.1
 */
@CompileStatic
class RustApplicationExec extends DefaultTask {

    /** Path to application executable.
     *
     * @return Path to exe.
     */
    @InputFile
    File getExecutable() {
        project.file(this.exe)
    }

    /** Set the executable.
     *
     * @param exe
     */
    void setExecutable(Object exe) {
        this.exe = exe
    }

    /** Set the executable.
     *
     * @param exe
     */
    void executable(Object exe) {
        this.exe = exe
    }

    @TaskAction
    void exec() {
        run(getExecutable())
    }

    @CompileDynamic
    private ExecResult run(final File execPath) {
        project.exec {
            executable execPath.absolutePath
        }
    }

    private Object exe
}
