//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.tasks.internal

import groovy.transform.CompileStatic
import org.ysb33r.gradle.rust.cargo.AbstractCargoRunnerTask

/**
 * @since 0.1
 */
@CompileStatic
abstract class AbstractRustLifecycleTask extends AbstractCargoRunnerTask {

    File getRustWorkingDir() {
        project.file(this.workingDir)
    }

    void setRustWorkingDir(Object f) {
        this.workingDir = f
    }

    void rustWorkingDir(Object f) {
        this.workingDir = f
    }

    @Override
    protected File getCargoWorkingDir() {
        getRustWorkingDir()
    }

    private Object workingDir

}
