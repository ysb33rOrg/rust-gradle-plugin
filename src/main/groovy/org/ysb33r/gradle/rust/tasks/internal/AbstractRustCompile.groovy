//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.tasks.internal

import groovy.transform.CompileStatic
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SkipWhenEmpty
import org.ysb33r.gradle.rust.cargo.AbstractCargoRunnerTask

/** Common compilation facilities for Rust.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractRustCompile extends AbstractRustLifecycleTask  {

    enum CompileType {
        EXE('--bins', 'src'),
        LIB('--lib', 'src'),
        TEST('--tests', 'tests'),
        BENCH('--benches', 'benches')

        final String cargoOption
        final String srcFolder

        CompileType(final String opt, final String folder) {
            cargoOption = opt
            srcFolder = folder
        }

        @Override
        String toString() {
            ":${cargoOption[2..-1]}"
        }
    }

    @Input
    CompileType getCompileType() {
        this.compileType
    }

    /** Directory where source code is to be found which can be compiled (after Gradle processing).
     *
     * @return Source code working directory
     */
    @Input
    File getRustSourceCodeWorkingDir() {
        project.file("${getCargoWorkingDir()}/${this.compileType.srcFolder}")
    }

    @InputFiles
    @SkipWhenEmpty
    FileCollection getSourceFiles() {
        project.fileTree(getRustSourceCodeWorkingDir())
    }

    /** Directory where binaries are written to.
     *
     * @return Directory where binaries are written to.
     */
    @OutputDirectory
    File getOutputDir() {
        project.file("${getCargoWorkingDir()}/target/${options.release ? 'release' : 'debug'}")
    }

    /** Set the compilation type for this task.
     *
     * @param type
     */
    void setCompileType(CompileType type) {
        this.compileType = type
    }

    @Override
    void exec() {
        super.exec()

        File errorOutput = cargoOutputFiles['error']

        if(errorOutput.exists()) {
            List<String> warnings = errorOutput.readLines().findAll { String line ->
                line.startsWith ('warning')
            }

            if(!warnings.empty) {
                logger.lifecycle( "${warnings.size()} warning${warnings.size() > 1 ? 's' : ''} found in compilation. See ${errorOutput.absolutePath} for details.")
            }
        }
    }

    @Override
    protected Iterable<String> getCargoCommandOptions() {
        [compileType.cargoOption]
    }

    private CompileType compileType
}
