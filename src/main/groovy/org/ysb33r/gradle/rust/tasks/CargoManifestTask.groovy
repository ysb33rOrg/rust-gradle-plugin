//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.tasks

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.rust.RustExtension
import org.ysb33r.gradle.rust.cargo.CargoManifest
import org.ysb33r.gradle.rust.cargo.internal.CargoPackage
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler
import org.ysb33r.gradle.rust.internal.dependencies.GradleDependencyHandlerUtils
import org.ysb33r.gradle.rust.plugins.RustBasePlugin

import static groovy.lang.Closure.DELEGATE_FIRST

/** Ability to create Cargo manifest files.
 *
 * @since 0.1
 */
class CargoManifestTask extends DefaultTask {

    @Nested
    CargoManifest getManifest() {
        this.manifest
    }

    void manifest(Closure configure) {
        Closure configurator = (Closure)(configure.clone())
        configurator.delegate = this.manifest
        configurator.resolveStrategy = DELEGATE_FIRST
        configurator()
    }

    void manifest(Action<CargoManifest> configurator) {
        configurator.execute(this.manifest)
    }
    
    @OutputFile
    File getOutputFile() {
        project.file this.outputFile
    }

    void setOutputFile(Object f) {
        this.outputFile = f
    }

    void outputFile(Object f) {
        this.outputFile = f
    }

    CargoManifestTask() {
        super()
        manifest = new CargoManifest(project)
        inputs.property 'dependencies', {
            RustDependencyHandler rust = GradleDependencyHandlerUtils.getExtensionByName(project,RustBasePlugin.DEPENDENCIES_EXT_NAME)
            [rust.compile.dependencies, rust.test.dependencies, rust.build.dependencies,
                rust.compile.targetDependencies, rust.test.targetDependencies, rust.build.targetDependencies]*.toString().join()
        }
    }

    @TaskAction
    void exec() {
        manifest.writeManifestTo(getOutputFile())
    }

    private Object outputFile
    private final CargoManifest manifest
}
