//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware

import static com.google.inject.Guice.createInjector

/**
 * @since 0.1
 */
@CompileStatic
class Injector {
    Injector(Project project) {
//        injector = createInjector(new RustGuiceModule(project))
    }

    /** Binds a service class to a project service
     *
     * @param target Service on {@code project} i.e. {@code project.dependencies}.
     * @param serviceName Name of item that will be attached to target
     * @param serviceClass Class of item that will be attached.
     */
//    void bindService(Object target, final String serviceName, Class serviceClass) {
//        ExtensionAware.cast(target).extensions.add(serviceName, injector.getInstance(serviceClass))
//    }

    void bindService(Object target, final String serviceName, Object service) {
        ExtensionAware.cast(target).extensions.add(serviceName, service)
    }

//    private final com.google.inject.Injector injector
}
