//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Copy
import org.ysb33r.gradle.rust.tasks.CargoManifestTask

/** Utility class to apply certain gradlesque Rust conventions.
 *
 * @since 0.1
 */
@CompileStatic
class RustConventions {

    /** Directory structure for Gradle conventions.
     *
     * @since 0.2
     */
    static final Map<String,String> GRADLE_SOURCE_LAYOUT = [
        src : 'src/main/rust',
        tests : 'src/test/rust',
        benches : 'src/benches/rust',
        docs : 'src/docs/rust',
        data : 'src/data/rust'
    ]

    /** Directory structure for Cargo conventions.
     *
     * @since 0.2
     */
    static final Map<String,String> CARGO_SOURCE_LAYOUT = [
        src : 'src',
        tests : 'tests',
        benches : 'benches',
        docs : 'doc',
        data : 'data'
    ]

    /** Apply standard source layout conventions to the task that will prepare a Rust source directory.
     *
     * Assumes {@code src/(main|test\benches)/rust} layout as the input.
     *
     * @param task Task that will copy Rust source code
     */
    @CompileDynamic
    static void applySourceCopyConventions(Copy task) {

        // TODO: Should really use sourcesets

        Closure layout = { final Project project, final String type ->
            project.rust.layout[type]
        }.curry(task.project)

        task.from layout.curry('src'), {
            include '**'
            into 'src'
        }
        task.from layout.curry('tests'), {
            include '**'
            into 'tests'
        }
        task.from layout.curry('benches'), {
            include '**'
            into 'benches'
        }
        task.from layout.curry('data'), {
            include '**'
            into 'data'
        }
        task.from layout.curry('docs'), {
            include '**'
            into 'doc'
        }
    }

    /** Creates a task that will copy Rust source code to a place where it can be dealt with by Cargo layout.
     *
     * @param project Project the task will be attached to.
     * @param name Name of task
     * @param relativeDir Directory relative to build directory where Rust source code will be placed.
     * @return The {@code Copy} task
     */
    static Copy createSourceCopyTask(final Project project, final String name, final String relativeDir) {
        Copy task = project.tasks.create(name, Copy)
        task.into("${project.buildDir}/${relativeDir}")
        task
    }

    /** Creates a task that will generate a Cargo manifest file.
     *
     * @param project Project the task will be attached to.
     * @param name Name of task
     * @param relativeDir Directory relative to build directory where temporary Rust manifests will be placed.
     * @param sourceSetName Name of Rust sourceset for which manifest is being created
     * @return The manifest task
     */
    static Task createManifestTask(
        final Project project,
        final String name,
        final String relativeDir,
        final String sourceSetName
    ) {
        CargoManifestTask task = project.tasks.create(name, CargoManifestTask)
        task.outputFile = "${project.buildDir}/${relativeDir}/${sourceSetName}/Cargo.toml"
        task
    }

    /** Returns a map of compilation task names as related to various phases
     * of the Rust development cycle.
     *
     * @param base Basename for a Rust sourceset. Defaults to {@code null} for the standard
     * Rust sourceset.
     *
     * @return Compilation task names by {@code exe}, {@code lib}, {@code test} & {@code bench} keys.
     */
    static Map<String,String> getCompileTaskNames( final String base = null) {
        final String name = base ? base.capitalize() : ''
        [
            exe: "compile${name}ExeRust".toString(),
            lib: "compile${name}LibRust".toString(),
            test: "compile${name}TestRust".toString(),
            bench: "compile${name}BenchesRust".toString()
        ]
    }

    /** Returns a map of test task names as related to the test and benchmark parts of
     * the Rust development cycle.
     *
     * @param base Basename for a Rust sourceset. Defaults to {@code null} for the standard
     * Rust sourceset.
     * @return Test task names by by {@code test} & {@code bench} keys/
     */
    static Map<String,String>  getTestTaskNames( final String base = null ) {
        final String name = base ? base.capitalize() : ''
        [
            test: "test${name}Rust".toString(),
            bench: "benchmark${name}Rust".toString()
        ]
    }

    /** Returns the name of the task that will run a compiled Rust application.
     *
     * @param base Basename for a Rust sourceset. Defaults to {@code null} for the standard
     * Rust sourceset.
     * @return Runner task name
     */
    static String getAppRunnerTaskName( final String base = null ) {
        final String name = base ? base.capitalize() : ''
        "run${name}App"
    }
}
