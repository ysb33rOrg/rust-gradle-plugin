//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.ysb33r.gradle.rust.dependencies.RustDependency

/** Specifies a patch.
 *
 * @since 0.1
 */
@CompileStatic
class PatchDependency extends ConfigurableDependency {

    final String repoName

    PatchDependency(final String repoName) {
        this.repoName = repoName
    }

    PatchDependency( final String repoName, final String name, final String path ) {
        this.repoName = repoName
        super.name(name)
        super.path(path)
    }

    /**
     * Returns whether two dependencies have identical values for their properties. A dependency is an entity with a
     * key. Therefore dependencies might be equal and yet have different properties.
     *
     * @param dependency The dependency to compare this dependency against.
     *
     * @since 0.2
     */
    @Override
    boolean contentEquals(Dependency dependency) {
        if(!dependency instanceof PatchDependency) {
            false
        } else {
            PatchDependency dep = (PatchDependency)(dependency)
            compare(dep) && dep.repoName == this.repoName
        }
    }
}
