//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.ysb33r.gradle.rust.dependencies.RustConfigurableDependency
import org.ysb33r.gradle.rust.dependencies.RustDependencyConfiguration
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler

import static groovy.lang.Closure.DELEGATE_FIRST

/** Implementation of a {@link org.ysb33r.gradle.rust.dependencies.RustDependencyHandler}.
 *
 * @since 0.1
 */
@CompileStatic
class DependencyHandler implements RustDependencyHandler {

    /** Way of referring to {@code crates.io} inside of {@code Cargo.toml}.
     *
     */
    static final String CRATES_IO_NAME = 'crates-io'

    /** Create a Rust dependency handler.
     *
     * @param project Project this Rust dependency handler is attached to.
     */
    DependencyHandler(Project project) {
        this.project = project
    }

    /** Return the Rust dependencies configuration.
     *
     */
    @Override
    RustDependencyConfiguration getCompile() {
        this.compileDeps
    }

    /** Return the Rust dev dependencies configuration.
     *
     */
    @Override
    RustDependencyConfiguration getTest() {
        this.testDeps
    }

    /** Return the Rust build dependencies configuration.
     *
     */
    @Override
    RustDependencyConfiguration getBuild() {
        this.buildDeps
    }

    /** Returns the container with all local patches.
     *
     * In Rust terms this is the {@code patch.crates-io} section.
     *
     * @return Container of patches.
     */
    @Override
    Iterable<PatchDependency> getPatches() {
        this.patches.dependencies as List<PatchDependency>
    }

    /** Map the local patches on a per repository-basis
     *
     * @return a map with all of the patches grouped by repository
     */
    @Override
    Map<String, Iterable<ConfigurableDependency>> getPatchesAsMap() {
        this.patches.dependencies.groupBy {
            ((PatchDependency)it).repoName
        } as Map< String, Iterable<ConfigurableDependency> >
    }

    /** Adds a dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    void compile(final String dep, final String... more) {
        this.compileDeps.add(dep)
        more.each { String it -> this.compileDeps.add it }
    }

    /** Adds a {@code dev} dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    @Override
    void test(final String dep, final String... more) {
        this.testDeps.add(dep)
        more.each { String it -> this.testDeps.add it }
    }

    /** Adds a {@code build} dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    @Override
    void build(final String dep, final String... more) {
        this.buildDeps.add(dep)
        more.each { String it -> this.buildDeps.add it }
    }

    /** Adds a dependency using a configurating closure.
     *
     * <code>
     *     compile {
     *         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path' )
     *     }
     * </code>
     *
     * @param depSpec
     */
    @Override
    void compile(@DelegatesTo(RustConfigurableDependency) Closure depSpec) {
        this.compileDeps.add(depSpec)
    }

    /** Adds a {@code dev} dependency using a configurating closure.
     *
     * <code>
     *     test {
     *         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path' )
     *     }
     * </code>
     *
     * @param depSpec
     */
    @Override
    void test(@DelegatesTo(RustConfigurableDependency) Closure depSpec) {
        this.testDeps.add(depSpec)
    }

    /** Adds a {@code dev} dependency using a configurating closure.
     *
     * <code>
     *     test {
     *         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path' )
     *     }
     * </code>
     *
     * @param depSpec
     */
    @Override
    void build(@DelegatesTo(RustConfigurableDependency) Closure depSpec) {
        this.buildDeps.add(depSpec)
    }

    @Override
    void compile(Action<RustConfigurableDependency> configurator) {
        this.compileDeps.add(configurator)
    }

    @Override
    void test(Action<RustConfigurableDependency> configurator) {
        this.testDeps.add(configurator)
    }

    @Override
    void build(Action<RustConfigurableDependency> configurator) {
        this.buildDeps.add(configurator)
    }

    /** Add a path to a patched dependency.
     *
     * @param name Name of dependency
     * @param path Local path to dependency
     */
    @Override
    void patch(final String name, final String path) {
        patches.add (new PatchDependency(CRATES_IO_NAME,name,path), {})
    }

    /** Add a path to a patched dependency.
     *
     * @param repoURI URI that is used for crates
     * @param name Name of dependency
     * @param path A relative path
     */
    @Override
    void patch(URI repoURI, String name, String path) {
        patches.add (new PatchDependency(repoURI.toString(),name,path), {})
    }

    /** Add a patched dependency via a configuration closure for a {@code crates.io} based crate.
     *
     * @param cfg Configurating closure
     */
    @Override
    void patch(@DelegatesTo(RustConfigurableDependency) Closure cfg) {
        patches.add( new PatchDependency(CRATES_IO_NAME), cfg)
    }

    /** Add a patched dependency via a configuration closure for a repository that is
     * not {@code crates.io}
     *
     * @param repoURI Repository URI
     * @param cfg Configurating closure
     */
    @Override
    void patch(URI repoURI, @DelegatesTo(RustConfigurableDependency) Closure cfg) {
        patches.add( new PatchDependency(repoURI.toString()), cfg)
    }

    /** Add a patched dependency via a configurating action for a {@code crates.io} based crate.
     *
     * @param cfg Configurating action.
     */
    @Override
    void patch(Action<RustConfigurableDependency> cfg) {
        patches.add( new PatchDependency(CRATES_IO_NAME), cfg)
    }

    /** Add a patched dependency via a configurating action for a repository that is not {@code crates.io}.
     *
     * @param repoURI Repository URI.
     * @param cfg Configurating action.
     */
    @Override
    void patch(URI repoURI, Action<RustConfigurableDependency> cfg) {
        patches.add( new PatchDependency(repoURI.toString()), cfg)
    }

    @Override
    void call(@DelegatesTo(RustDependencyHandler) Closure cfg) {
        configure(cfg)
    }

    @Override
    void configure(@DelegatesTo(RustDependencyHandler) Closure cfg) {
        Closure configurator = (Closure) (cfg.clone())
        configurator.resolveStrategy = DELEGATE_FIRST
        configurator.delegate = this
        configurator()
    }

    @Override
    void configure(Action<RustDependencyHandler> cfg) {
        cfg.execute(this)
    }

    private final Project project
    private final DependencyContainer compileDeps = new DependencyContainer('dependencies')
    private final DependencyContainer testDeps = new DependencyContainer('dev-dependencies')
    private final DependencyContainer buildDeps = new DependencyContainer('build-dependencies')
    private final DependencyContainer patches = new DependencyContainer('patches')

}
