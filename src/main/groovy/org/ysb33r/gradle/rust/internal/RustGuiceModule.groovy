//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal

import com.google.inject.AbstractModule
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.internal.project.DefaultProject
import org.gradle.internal.service.ServiceRegistry

/**
 * @since 0.1
 */
@CompileStatic
class RustGuiceModule extends AbstractModule {

    RustGuiceModule(Project project) {
        this.project = project;
        this.serviceRegistry = ((DefaultProject)project).services
    }

    @Override
    protected void configure() {
        bind(Project).toInstance(project)
        bind(ServiceRegistry).toInstance(serviceRegistry)
    }

    private final Project project
    private final ServiceRegistry serviceRegistry

}
