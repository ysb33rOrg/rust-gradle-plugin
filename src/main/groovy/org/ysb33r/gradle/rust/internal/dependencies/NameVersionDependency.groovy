//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.Dependency
import org.ysb33r.gradle.rust.dependencies.RustDependency

/** Defines a simple name-version Rust dependency.
 *
 * @since 0.1
 */
@CompileStatic
class NameVersionDependency implements RustDependency {

    final String name
    final String version

    NameVersionDependency(final String name, final String version) {
        this.name = name
        this.version = version
    }

    /** Indicates whether the dependency is platform-specific
     *
     * @return Alqays {@code null} as this syntax does not allow for targets.
     *
     */
    @Override
    String getTarget() {
        null
    }

    /** Short-form dependencies cannot have features configured.
     *
     * @return {@code false} .
     *
     * @since 0.2
     */
    @Override
    boolean getHasFeatures() {
        false
    }

    /** Short-form dependencies cannot have features configured.
     *
     * @return TH elist of features requested. Can be empty, but never null.
     *
     * @since 0.2
     */
    @Override
    List<String> getFeatures() {
        NO_FEATURES
    }

    /** Returns the group of this dependency. The group is often required to find the artifacts of a dependency in a
     * repository. For example, the group name corresponds to a directory name in a Maven like repository. Might return
     * null.
     *
     * @since 0.2
     */
    @Override
    String getGroup() {
        null
    }

    /**
     * Returns whether two dependencies have identical values for their properties. A dependency is an entity with a
     * key. Therefore dependencies might be equal and yet have different properties.
     *
     * @param dependency The dependency to compare this dependency with
     *
     * @since 0.2
     */
    @Override
    boolean contentEquals(Dependency dependency) {
        boolean basicMatch = this.name == dependency.name && this.version == dependency.version
        if (basicMatch) {
            false
        } else if (dependency instanceof NameVersionDependency) {
            true
        } else if (dependency instanceof PatchDependency) {
            false
        } else if (dependency instanceof ConfigurableDependency) {
            ConfigurableDependency dep = (ConfigurableDependency) dependency
            !dep.hasFeatures && toString() == dep.toString()
        } else {
            false
        }
    }

    /**
     * Creates and returns a new dependency with the property values of this one.
     *
     * @return The copy. Never returns null.
     *
     * @since 0.2
     */
    @Override
    Dependency copy() {
        new NameVersionDependency(this.name, this.version)
    }

    /**
     * Returns a reason why this dependency should be used, in particular with regards to its version. The dependency report
     * will use it to explain why a specific dependency was selected, or why a specific dependency version was used.
     *
     * @return a reason to use this dependency
     *
     * @since 0.2
     */
    @Override
    String getReason() {
        this.reason
    }

    /** Sets the reason why this dependency should be used.
     *
     * @since 0.2
     */
    @Override
    void because(String reason) {
        this.reason = reason
    }

    /** Displays the dependency in a way that is also acceptable to {@code Cargo.toml}.
     *
     * @return Dependency as a string.
     */
    @Override
    String toString() {
        "${name} = \"${version}\""
    }

    private final static List<String> NO_FEATURES = []
    private String reason = 'Provided Cargo dependency'
}
