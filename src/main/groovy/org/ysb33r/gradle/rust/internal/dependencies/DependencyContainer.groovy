//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.ysb33r.gradle.rust.dependencies.RustConfigurableDependency
import org.ysb33r.gradle.rust.dependencies.RustDependency
import org.ysb33r.gradle.rust.dependencies.RustDependencyConfiguration
import org.ysb33r.gradle.rust.dependencies.RustDependencyFormatException

import static groovy.lang.Closure.DELEGATE_FIRST

/** Cnotains the dependencies for a specific Rust configuration.
 *
 * @since 0.1
 */
@CompileStatic
class DependencyContainer implements RustDependencyConfiguration {

    DependencyContainer(final String name) {
        this.name = name
    }

    @Override
    void add(final String s) {
        final String dep = s.startsWith(':') ? s.substring(1) : s

        String[] parts = dep.split(':')

        if (parts.size() != 2) {
            throw new RustDependencyFormatException('Gradle-Rust dependency strings should be in "name:version: format')
        }

        dependencies.add(new NameVersionDependency(parts[0], parts[1]))
    }

    /** Creates, configures and adds a dependency using a configurating closure.
     *
     * @param depSpec
     */
    @Override
    void add(Closure depSpec) {
        ConfigurableDependency dep = new ConfigurableDependency()
        Closure spec = (Closure) (depSpec.clone())
        spec.resolveStrategy = DELEGATE_FIRST
        spec.delegate = dep
        spec.call()
        dependencies.add(dep)
    }

    /** Configures and adds a dependency using a configurating closure.
     *
     * @param dep Dependency to add
     * @param depSpec Specification for configuring the dependency
     */
    void add(ConfigurableDependency dep, Closure depSpec) {
        Closure spec = (Closure) (depSpec.clone())
        spec.resolveStrategy = DELEGATE_FIRST
        spec.delegate = dep
        spec.call()
        dependencies.add(dep)
    }

    /** Creates, configures and adds a dependency using a configurating action.
     *
     * @param depSpec Specification to configure newly created dependency/
     */
    @Override
    void add(Action<RustConfigurableDependency> depSpec) {
        ConfigurableDependency dep = new ConfigurableDependency()
        add(dep, depSpec)
    }

    /** Adds a previously created dependency to this group.
     *
     * @param dep Previously created dependency.
     *
     * @since 0.2
     */
    @Override
    void add(RustDependency dep) {
        dependencies.add(dep)
    }

    /** Configures and adds a dependency using a configurating action.
     *
     * @param dep Dependency to add
     * @param depSpec Specification to configuare supplied dependency
     */
    void add(ConfigurableDependency dep, Action<RustConfigurableDependency> depSpec) {
        depSpec.execute(dep)
        dependencies.add(dep)
    }

    @Override
    Iterable<RustDependency> getDependencies() {
        this.dependencies.findAll {
            if(it instanceof ConfigurableDependency) {
                ((ConfigurableDependency)(it)).target == null
            } else {
                true
            }
        }
    }

    /** Dependencies that are Target (platform) specific.
     *
     * @return Rust dependencies grouped by target.
     */
    @Override
    Map<String, Iterable<RustDependency>> getTargetDependencies() {
        this.dependencies.findAll {
            it instanceof ConfigurableDependency && ((ConfigurableDependency)(it)).target
        }.groupBy {
            ((ConfigurableDependency)(it)).target
        } as Map<String, Iterable<RustDependency> >
    }

    @Override
    String getName() {
        this.name
    }

    private final List<RustDependency> dependencies = []
    private final String name
}
