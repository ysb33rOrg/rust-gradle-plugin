//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.Dependency
import org.ysb33r.gradle.rust.dependencies.RustConfigurableDependency
import org.ysb33r.gradle.rust.dependencies.RustDependency
import org.ysb33r.gradle.rust.dependencies.RustDependencyFormatException
import org.ysb33r.grolifant.api.StringUtils

/**
 * @since 0.1
 */
@CompileStatic
class ConfigurableDependency implements RustDependency, RustConfigurableDependency {

    /** Set name of configurable dependency.
     *
     * @param name Name of dependency.
     */
    void name( final String name) {
        this.name = name
    }

    /** Set version of configurable dependency.
     *
     * @param name Version of dependency.
     */
    void version( final String version) {
        this.version = version
    }

    /** Define a Git repository where dependency is stored.
     *
     * @param repo Git repository
     */
    void git( final String repo ) {
        if(path) {
            throw new RustDependencyFormatException("Cannot set git repo if path is configured")
        }
        this.gitRepo = repo
    }

    /** Define a Git repository and associated branch where dependency is stored.
     *
     * @param repo Git repository
     * @param branch Git branch
     */
    void git( final String repo, final String branch) {
        if(path) {
            throw new RustDependencyFormatException("Cannot set git repo if path is configured")
        }
        this.gitRepo = repo
        this.gitBranch = branch
    }

    /** Define a local path where dependency can be found.
     *
     * @param path Path in string form.
     */
    void path( final String path) {
        if(gitRepo) {
            throw new RustDependencyFormatException("Cannot set path if git repo is configured")
        }
        this.path = path
    }

    /** Sets a target platform to which the dependency will apply.
     *
     * @param platform Target platform in a format that is recognisable by {@code cargo}.
     */
    void target( final String platform ) {
        this.target = platform
    }

    /** Get the associated target platform.
     *
     * @return Target platform or {@code null} is this is a platform-agnostic dependency.
     */
    String getTarget() {
        this.target
    }

    /** If the dependency has optional features, set whether the default features should be included.
     *
     * @param flag {@code false} to not include the default features.
     */
    void setWithDefaultFeatures(boolean flag) {
        this.withDefaultFeatures = flag
    }

    /** If the dependency has optional features, set whether the default features should be included.
     *
     * This is an alternate form to allow a declarative rather than assignment DSL.
     * @param flag {@code false} to not include the default features.
     */
    void withDefaultFeatures(boolean flag) {
        this.withDefaultFeatures = flag
    }

    /** Returns whether default features should be included.
     *
     * @return If previously set, return that value, otherwise {@code true} by default.
     */
    @Override
    boolean getWithDefaultFeatures() {
        this.withDefaultFeatures
    }

    /** If a dependency has optional features, set the list of features to be included.
     *
     * @param featureList Feature list as anything that can be converted to a string by {@link org.ysb33r.grolifant.api.StringUtils}.
     */
    void setFeatures( Iterable<Object> featureList ) {
        this.features.clear()
        this.features.addAll (featureList)
    }

    /** If a dependency has optional features, add to the list of features to be included.
     *
     * @param featureList Feature list as anything that can be converted to a string by {@link org.ysb33r.grolifant.api.StringUtils}.
     */
    void features( Iterable<Object> featureList ) {
        this.features.addAll (featureList)
    }

    /** If a dependency has optional features, add to the list of features to be included.
     *
     * @param featureList Feature list as anything that can be converted to a string by {@link org.ysb33r.grolifant.api.StringUtils}.
     */
    void features( Object... featureList) {
        this.features.addAll (featureList)
    }

    /** List of optional features previsouly set by the user.
     *
     * @return List of optional features be included for this dependency.
     */
    List<String> getFeatures() {
        StringUtils.stringize(this.features)
    }

    /** Indicates whether optional features has been requested.
     *
     * @return For this to return {@code true}, {@code withDefaultFeatures} must be {@code false} or the {@code features} must have been called previously with a non-empty list.
     */
    boolean getHasFeatures() {
        !this.withDefaultFeatures || !this.features.empty
    }

    /** Returns the dependency name in a form that is recognisable by {@code Cargo.toml}.
     *
     * @return String representation.
     */
    @Override
    String toString() {
        if( !name ) {
            throw new RustDependencyFormatException("Name has not been set")
        }
        if(gitRepo) {
            if(gitBranch) {
                "${name} = { git = \"${gitRepo}\", branch = \"${gitBranch}\" }"
            } else {
                "${name} = { git = \"${gitRepo}\" }"
            }
        } else if (path){
            if(version) {
                "${name} = { path = \"${path}\", version = \"${version}\" }"
            } else {
                "${name} = { path = \"${path}\" }"
            }
        } else if (name && !version) {
            throw new RustDependencyFormatException("Name has been set without a version")
        } else {
            "${name} = \"${version}\""
        }
    }

    /** Returns the group of this dependency.
     *
     * @return Always {@code null}.
     *
     * @since 0.2
     */
    @Override
    String getGroup() {
        null
    }

    /** Returns the name of this dependency. The name is almost always required to find the artifacts of a dependency in
     * a repository. Never returns null.
     *
     * @since 0.2
     */
    @Override
    String getName() {
        if(this.name == null) {
            throw new RustDependencyFormatException("Name not set")
        }
        this.name
    }

    /** Returns the version of this dependency.
     *
     * @return Version of dependency. Can be {@code null}.
     *
     * @since 0.2
     */
    @Override
    String getVersion() {
        this.version
    }

    /** Returns whether two dependencies have identical values for their properties. A dependency is an entity with a
     * key. Therefore dependencies might be equal and yet have different properties.
     *
     * @param dependency The dependency to compare this dependency with
     *
     * @since 0.2
     */
    @Override
    boolean contentEquals(Dependency dependency) {
        if(dependency instanceof NameVersionDependency) {
            ((NameVersionDependency)dependency).contentEquals(this)
        } else if (dependency instanceof ConfigurableDependency && !dependency instanceof PatchDependency) {
            compare((ConfigurableDependency)dependency)
        } else {
            false
        }
    }

    /** Creates and returns a new {@code ConfigurableDependency} with the property values of this one.
     *
     * @return The copy. Never returns null.
     *
     * @since 0.2
     */
    @Override
    Dependency copy() {
        ConfigurableDependency ret = new ConfigurableDependency()

        if(this.name) {
            ret.name(this.name)
        }

        if(this.version) {
            ret.version(this.version)
        }

        if(this.gitRepo) {
            ret.gitRepo = this.gitRepo
        }

        if(this.gitBranch) {
            ret.gitBranch = this.gitBranch
        }

        if(this.path) {
            ret.path = this.path
        }

        if(this.target) {
            ret.target(this.target)
        }

        ret.withDefaultFeatures = this.withDefaultFeatures
        ret.setFeatures(this.features)

        ret
    }

    /** Returns a reason why this dependency should be used, in particular with regards to its version. The dependency report
     * will use it to explain why a specific dependency was selected, or why a specific dependency version was used.
     *
     * @return a reason to use this dependency.
     *
     * @since 0.2
     */
    @Override
    String getReason() {
        this.reason
    }

    /**
     * Sets the reason why this dependency should be used.
     *
     * @reason Provide a reason.
     *
     * @since 0.2
     */
    @Override
    void because(String reason) {
        this.reason = reason
    }

    /** Base method for comparing to instances of {@code ConfigurableDependency}.
     *
     * @param dep Other dependency
     * @return Whether the content of the two dependencies is equal.
     *
     * @since 0.2
     */
    protected boolean compare(ConfigurableDependency dep) {
        this.name == dep.name && this.version == dep.version && this.gitRepo == dep.gitRepo &&
            this.gitBranch == dep.gitBranch && this.withDefaultFeatures == dep.withDefaultFeatures &&
            this.path == dep.path && this.target == dep.target &&
            this.features.containsAll(dep.features) && dep.features.containsAll(this.features)
    }

    private String name
    private String version
    private String gitRepo
    private String gitBranch
    private String path
    private String target
    private boolean withDefaultFeatures = true
    private final List<Object> features = []
    private String reason = 'Specified Cargo dependency'

}
