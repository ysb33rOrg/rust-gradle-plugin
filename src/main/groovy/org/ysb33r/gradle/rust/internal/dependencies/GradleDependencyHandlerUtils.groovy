//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.internal.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler

/** Abstracts Gradle internals away from rest of plugin.
 *
 * @since 0.1
 */
@CompileStatic
class GradleDependencyHandlerUtils {

    static void addExtension(final Project project, final String name, RustDependencyHandler extension ) {
        ExtensionAware.cast(project.dependencies).extensions.add(name, extension)
    }

    static RustDependencyHandler getExtensionByName(final Project project, final String name) {
        (RustDependencyHandler)(ExtensionAware.cast(project.dependencies).extensions.getByName(name))
    }
}
