//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.plugins.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Copy
import org.ysb33r.gradle.rust.internal.RustConventions
import org.ysb33r.gradle.rust.plugins.RustBasePlugin
import org.ysb33r.gradle.rust.tasks.RustCompile
import org.ysb33r.gradle.rust.tasks.RustTestTask
import org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile

import static org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile.CompileType.TEST

/** Common functionality between Rust library and application plugins.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractRustPlugin implements Plugin<Project> {

    static final String MANIFEST_TASK_NAME = 'cargoManifest'
    static final Map<String,String> DEFAULT_TASK_NAMES = RustConventions.getCompileTaskNames()
    static final Map<String,String> DEFAULT_TEST_NAMES = RustConventions.getTestTaskNames()

    static final String TEST_TASK_NAME
    protected void applyBase(Project project) {
        project.apply plugin : RustBasePlugin

        Copy sourceCopyTask = (Copy)(project.tasks.getByName(RustBasePlugin.SOURCE_COPY_TASK))
        RustConventions.applySourceCopyConventions( sourceCopyTask )

        Task manifest = createManifestTask(project)
        Task assemble = project.tasks.getByName('assemble')
        Task check = project.tasks.getByName('check')

        RustCompile rustCompile = createRustCompileTask(project)
        rustCompile.dependsOn sourceCopyTask
        rustCompile.rustWorkingDir sourceCopyTask.destinationDir
        assemble.dependsOn(rustCompile)

        RustCompile testCompileTask = project.tasks.create(AbstractRustPlugin.DEFAULT_TASK_NAMES['test'], RustCompile)
        testCompileTask.compileType = TEST
        testCompileTask.dependsOn sourceCopyTask, rustCompile
        testCompileTask.rustWorkingDir sourceCopyTask.destinationDir

        sourceCopyTask.from manifest

        RustTestTask testTask = createRustTestTask(project)
        testTask.dependsOn testCompileTask
        testTask.mustRunAfter rustCompile
        testTask.rustWorkingDir sourceCopyTask.destinationDir
        testTask.inputs.dir(testCompileTask.rustSourceCodeWorkingDir).skipWhenEmpty()
        check.dependsOn testTask

    }

    abstract protected RustCompile createRustCompileTask(Project project)

    private RustTestTask createRustTestTask(Project project) {
        project.tasks.create(
            DEFAULT_TEST_NAMES['test'],
            RustTestTask
        )
    }

    private Task createManifestTask(Project project) {
        Task task = RustConventions.createManifestTask(project,MANIFEST_TASK_NAME,'cargo-manifests', 'main')
        task.description = 'Creates Cargo manifest'
        task
    }

}
