//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.ysb33r.gradle.rust.plugins.internal.AbstractRustPlugin
import org.ysb33r.gradle.rust.tasks.RustCompile

import static org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile.CompileType.EXE
import static org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile.CompileType.LIB

/** Adds tasks to build a Rust-based libraries
 *
 * @since 0.1
 */
@CompileStatic
class RustLibraryPlugin extends AbstractRustPlugin {
    @Override
    void apply(Project project) {
        super.applyBase(project)
    }

    @Override
    protected RustCompile createRustCompileTask(Project project) {
        RustCompile task = project.tasks.create(AbstractRustPlugin.DEFAULT_TASK_NAMES['lib'], RustCompile)
        task.compileType = LIB
        task
    }

}
