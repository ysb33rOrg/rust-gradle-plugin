//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.ysb33r.gradle.rust.RustExtension
import org.ysb33r.gradle.rust.cargo.internal.CargoTomlParser
import org.ysb33r.gradle.rust.internal.Injector
import org.ysb33r.gradle.rust.internal.RustConventions
import org.ysb33r.gradle.rust.internal.dependencies.DependencyHandler
import org.ysb33r.gradle.rust.plugins.internal.AbstractRustPlugin
import org.ysb33r.gradle.rust.tasks.CargoManifestTask

/** Provides core functionality for working with Rust & Cargo.
 *
 * @since 0.1
 */
@CompileStatic
class RustBasePlugin implements Plugin<Project> {

    static final String SOURCE_COPY_TASK = 'processRustSource'
    static final String RUST_PROJECT_WORKDIR = 'rust-project'
    static final String DEPENDENCIES_EXT_NAME = 'rust'

    @Override
    void apply(Project project) {
        project.apply plugin: LifecycleBasePlugin

        DependencyHandler rustDependencyHandler = new DependencyHandler(project)
        Injector injector = new Injector(project)
        injector.bindService(project.dependencies, DEPENDENCIES_EXT_NAME, rustDependencyHandler)

        RustExtension rustExtension = (RustExtension) (project.extensions.create(RustExtension.NAME, RustExtension, project))
        Copy sourceCopyTask = RustConventions.createSourceCopyTask(project, SOURCE_COPY_TASK, RUST_PROJECT_WORKDIR)
        sourceCopyTask.description = 'Process Rust source before compilation'

        project.afterEvaluate { Project p ->
            File cargoToml = rustExtension.cargoToml

            if(cargoToml) {
                CargoManifestTask manifest = (CargoManifestTask)(p.tasks.findByName(AbstractRustPlugin.MANIFEST_TASK_NAME))
                CargoTomlParser parser = new CargoTomlParser(project,manifest?.manifest?.package,rustDependencyHandler)
                parser.parseFile(cargoToml)
            }
        }

    }

}
