//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Exec
import org.ysb33r.gradle.rust.internal.RustConventions
import org.ysb33r.gradle.rust.internal.RustInstaller
import org.ysb33r.gradle.rust.plugins.internal.AbstractRustPlugin
import org.ysb33r.gradle.rust.tasks.CargoManifestTask
import org.ysb33r.gradle.rust.tasks.RustApplicationExec
import org.ysb33r.gradle.rust.tasks.RustCompile
import org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile

import static org.ysb33r.gradle.rust.tasks.internal.AbstractRustCompile.CompileType.EXE

/** Adds tasks to build a Rust-based application
 *
 * @since 0.1
 */
@CompileStatic
class RustApplicationPlugin extends AbstractRustPlugin {

    @Override
    void apply(Project project) {
        super.applyBase(project)
        createRustRunnerTask(project)
    }

    @Override
    protected RustCompile createRustCompileTask(Project project) {
        RustCompile task = project.tasks.create(AbstractRustPlugin.DEFAULT_TASK_NAMES['exe'], RustCompile)
        task.compileType = EXE
        task
    }

    private Task createRustRunnerTask(Project project) {

        RustApplicationExec runner = project.tasks.create(
            RustConventions.getAppRunnerTaskName(),
            RustApplicationExec
        )

        runner.executable = { ->
            final CargoManifestTask manifestTask = (CargoManifestTask)(project.tasks.getByName(AbstractRustPlugin.MANIFEST_TASK_NAME))
            final RustCompile compileTask = (RustCompile)(project.tasks.getByName(AbstractRustPlugin.DEFAULT_TASK_NAMES['exe']))
            final String exeName = "${manifestTask.manifest.package.name}${RustInstaller.OS.isWindows() ? '.exe' : ''}"
            new File(compileTask.outputDir, exeName)
        }

        runner.dependsOn AbstractRustPlugin.DEFAULT_TASK_NAMES['exe']
        runner
    }
}
