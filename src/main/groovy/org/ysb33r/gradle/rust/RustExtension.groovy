//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.ysb33r.gradle.rust.internal.RustConventions
import org.ysb33r.gradle.rust.internal.RustInstaller
import org.ysb33r.grolifant.api.exec.AbstractToolExtension
import org.ysb33r.grolifant.api.exec.ResolveExecutableByVersion

/**
 * @since 0.1
 */
@CompileStatic
class RustExtension extends AbstractToolExtension {

    static final String RUST_DEFAULT = '1.24.1'
    static final String NAME = 'rust'

    RustExtension(Project project) {
        super(project)
        addVersionResolver(project)
        executable([version: RUST_DEFAULT] as Map<String, Object>)
        this.abiToolsSearchPaths = RustInstaller.OS.path as List<Object>
    }

    /** Use this to configure a system path search for {@code Packer}.
     *
     * @return Returns a special option to be used in {@link #executable}
     */
    static Map<String, Object> searchPath() {
        RustExtension.SEARCH_PATH
    }

    /** Resolves the path to the {@code cargo} executable.
     *
     * @return Path to {@code cargo}.
     */
    File resolveCargoPath() {
        new File( resolveRustcPath().parentFile, RustInstaller.OS.isWindows() ? 'cargo.exe' : 'cargo' )
    }

    /** Resolves the path to the {@code rustc} executable.
     *
     * @return Path to {@code rustc}.
     */
    File resolveRustcPath() {
        resolvableExecutable.executable
    }

    /** Set the list of paths to search for linker etc. in the operating system format
     *
     */
    void setAbiToolsSearchPath(Iterable<Object> paths) {
        this.abiToolsSearchPaths.clear()
        this.abiToolsSearchPaths.addAll(paths)
    }

    /** Add to list of paths to search for linker etc. in the operating system format
     *
     */
    void abiToolsSearchPath(Iterable<Object> paths) {
        this.abiToolsSearchPaths.addAll(paths)
    }

    /** Add to list of paths to search for linker etc. in the operating system format
     *
     */
    void abiToolsSearchPath(Object... paths) {
        this.abiToolsSearchPaths.addAll(paths)
    }

    /** Returns a list of paths to search for linker etc. in the operating system format.
     *
     * If not configured explicitly will return the system search path.
     *
     * @return Search path as file collection
     */
    FileCollection getAbiToolsSearchPath() {
        project.files(this.abiToolsSearchPaths ?: RustInstaller.OS.path)
    }

    /** If a {@code Cargo.toml} file should be used to augment configuration, this is the place to configure its location.
     *
     * @param cargoTomlLocation ANythging that can be converetd with Gradle's {@code project.file} method.
     *
     * @since 0.2
     */
    void setCargoToml(Object cargoTomlLocation) {
        this.cargoToml = cargoTomlLocation
    }

    /** If a {@code Cargo.toml} file should be used to augment configuration, this is the place to configure its location.
     *
     * @param cargoTomlLocation ANythging that can be converetd with Gradle's {@code project.file} method.
     *
     * @since 0.2
     */
    void cargoToml(Object cargoTomlLocation) {
        this.cargoToml = cargoTomlLocation
    }

    /** Returns the {@code Cargo.toml} file location to read information from.
     *
     * @return File location of {@code null} if no file should be read.
     *
     * @since 0.2
     */
    File getCargoToml() {
        this.cargoToml ? project.file(this.cargoToml) : null
    }

    /** Use Gradle conventions for laying out Rust source.
     *
     * This is the default convention.
     *
     * @since 0.2
     */
    void useGradleSourceLayout() {
        this.layout = RustConventions.GRADLE_SOURCE_LAYOUT as Map<String,Object>
    }

    /** Use Cargo conventions for laying out Rust source.
     *
     * @since 0.2
     */
    void useCargoSourceLayout() {
        this.layout = RustConventions.CARGO_SOURCE_LAYOUT as Map<String,Object>
    }

    /** Use Cargo conventions for laying out Rust source.
     *
     * This is useful if you do not want the Rust project to be at the same level as the Gradle project.
     *
     * @oaram baseDir Parent directory above Cargo-layout Rust project. Can be anything convertible via
     * Gradle's {@code project.file}
     *
     * @since 0.2
     */
    void useCargoSourceLayout(Object baseDir) {
        this.layout = RustConventions.CARGO_SOURCE_LAYOUT.collectEntries { String key, String path ->
            [ key.toString(), new File(project.file(baseDir),path) ]
        } as Map<String,Object>
    }

    /** Returns a representation of the Ryst source code layout.
     *
     * @return Key-based directory layout.
     *
     * @since 0.2
     */
    Map<String,Object> getLayout() {
        this.layout
    }

    private void addVersionResolver(Project project) {

        ResolveExecutableByVersion.DownloaderFactory downloaderFactory = {
            Map<String, Object> options, String version, Project p ->
                new RustInstaller(p, version)
        } as ResolveExecutableByVersion.DownloaderFactory

        ResolveExecutableByVersion.DownloadedExecutable resolver = { RustInstaller installer ->
            installer.getRustcExecutablePath()
        } as ResolveExecutableByVersion.DownloadedExecutable

        getResolverFactoryRegistry().registerExecutableKeyActions(
            new ResolveExecutableByVersion(project, downloaderFactory, resolver)
        )
    }

    private final List<Object> abiToolsSearchPaths
    private static final Map<String, Object> SEARCH_PATH = [search: 'rustc'] as Map<String, Object>
    private Object cargoToml

    // TODO: Should really be sourceset-based.
    private Map<String,Object> layout = RustConventions.GRADLE_SOURCE_LAYOUT as Map<String,Object>
}
