//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Named

/** Contains a collection of Rust dependencies
 *
 * @since 0.1
 */
@CompileStatic
interface RustDependencyConfiguration extends Named {

    /** Adds a dependency using a simple test description.
     *
     * @param s
     */
    void add(String s)

    /** Adds a dependency using a configurating closure.
     *
     * @param depSpec
     */
    void add(Closure depSpec)

    /** Adds a dependency using a configurating action.
     *
     * @param depSpec
     */
    void add(Action<RustConfigurableDependency> depSpec)

    /** Adds a previsouly created dependency to this group.
     *
     * @param dep Previously created dependency.
     *
     * @since 0.2
     */
    void add(final RustDependency dep)

    /** Returns all dependencies that are not target (platform) specific.
     *
     * @return All platform-independent dependencies .
     */
    Iterable<RustDependency> getDependencies()

    /** Dependencies that are Target (platform) specific.
     *
     * @return Rust dependencies grouped by target.
     */
    Map< String, Iterable<RustDependency> > getTargetDependencies()
}