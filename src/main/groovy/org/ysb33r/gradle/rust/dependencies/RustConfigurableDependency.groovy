//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.dependencies

import groovy.transform.CompileStatic

/** A Rust configurable dependency can specify all the options that can be specified via
 * {@code Cargo.toml}.
 *
 * @since 0.1
 */
@CompileStatic
interface RustConfigurableDependency {

    void name(final String name)

    void version(final String version)

    void git(final String repo)

    void git(final String repo, final String branch)

    void path(final String path)

    void target(final String platform)

    String getTarget()

    /** Returns whether default features should be included or not.
     *
     * @return {@code true} it they shoul dbe included.
     * @since 0.2
     */
    boolean getWithDefaultFeatures()
}