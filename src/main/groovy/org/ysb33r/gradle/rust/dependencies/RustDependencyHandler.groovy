//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.ysb33r.gradle.rust.internal.dependencies.ConfigurableDependency
import org.ysb33r.gradle.rust.internal.dependencies.PatchDependency

/**
 * @since 0.1
 */
@CompileStatic
interface RustDependencyHandler {

    /** Returns the container will all compile dependencies.
     *
     * @return Container of dependencies
     */
    RustDependencyConfiguration getCompile()

    /** Returns the container will all test dependencies.
     *
     * In Rust terms, this is the '{@code dev}' dependencies.
     *
     * @return Container of dependencies.
     */
    RustDependencyConfiguration getTest()

    /** Returns the container will all build dependencies.
     *
     * In Rust terms, this is the '{@code build}' dependencies.
     *
     * @return Container of dependencies.
     */
    RustDependencyConfiguration getBuild()

    /** Returns the container with all local patches.
     *
     * In Rust terms this is the {@code patch.crates-io} section.
     *
     * @return Container of patches.
     */
    Iterable<PatchDependency> getPatches()

    /** Map the local patches on a per repository-basis
     *
     * @return a map with all of the patches grouped by repository
     */
    Map<String, Iterable<ConfigurableDependency>> getPatchesAsMap()

    /** Adds a dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    void compile(final String dep, final String... more)

    /** Adds a {@code dev} dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    void test(final String dep, final String... more)

    /** Adds a {@code build} dependency in the format '{@code name:version}'.
     *
     * @param dep Dependency
     * @param more Additional dependencies
     */
    void build(final String dep, final String... more)

    /** Adds a dependency using a configurating closure.
     *
     * <code>
     *     compile {*         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path'
     *}* </code>
     *
     * @param depSpec
     */
    void compile(Closure depSpec)

    /** Adds a {@code dev} dependency using a configurating closure.
     *
     * <code>
     *     test {*         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path' )
     *}* </code>
     *
     * @param depSpec
     */
    void test(Closure depSpec)

    /** Adds a {@code dev} dependency using a configurating closure.
     *
     * <code>
     *     test {*         name 'winhttp'
     *         version '1.2.3'
     *         git( 'http://github.com/foo/bar' )
     *         git( 'http://github.com/foo/bar', 'my-branch')
     *         path( 'some/path' )
     *}* </code>
     *
     * @param depSpec
     */
    void build(Closure depSpec)

    /** Add a dependency via an Gradle {@code Action}
     *
     * @param configurator Action that will configure a {@link RustConfigurableDependency}.
     */
    void compile(Action<RustConfigurableDependency> configurator)

    /** Add a {@code dev} dependency via an Gradle {@code Action}
     *
     * @param configurator Action that will configure a {@link RustConfigurableDependency}.
     */
    void test(Action<RustConfigurableDependency> configurator)

    /** Add a {@code build} dependency via an Gradle {@code Action}
     *
     * @param configurator Action that will configure a {@link RustConfigurableDependency}.
     */
    void build(Action<RustConfigurableDependency> configurator)

    /** Add a path to a patched dependency.
     *
     * @param name Name of dependency
     * @param path A relative path
     */
    void patch(final String name, final String path)

    /** Add a path to a patched dependency.
     *
     * @param repoURI URI that is used for crates
     * @param name Name of dependency
     * @param path A relative path
     */
    void patch(final URI repoURI, final String name, final String path)

    /** Add a patched dependency via a configuration closure for a {@code crates.io} based crate.
     *
     * @param cfg Configurating closure
     */
    void patch(Closure cfg)

    /** Add a patched dependency via a configuration closure for a repository that is
     * not {@code crates.io}
     *
     * @param repoURI Repository URI
     * @param cfg Configurating closure
     */
    void patch(final URI repoURI, Closure cfg)

    /** Add a patched dependency via a configurating action for a {@code crates.io} based crate.
     *
     * @param cfg Configurating action.
     */
    void patch(Action<RustConfigurableDependency> cfg)

    /** Add a patched dependency via a configurating action for a repository that is not {@code crates.io}.
     *
     * @param repoURI Repository URI.
     * @param cfg Configurating action.
     */
    void patch(final URI repoURI, Action<RustConfigurableDependency> cfg)

    /** Groovy extension that will pass any configurating closure
     *
     * @param cfg Closure to configure dependency handler.
     */
    void call(Closure cfg)

    /** Configure the handler via a Groovy closure.
     *
     * @param cfg Closure to configure dependency handler.
     */
    void configure(Closure cfg)

    /** Configure th handler via a Gradle action.
     *
     * @param cfg {@code Action} to configure dependency handler.
     */
    void configure(Action<RustDependencyHandler> cfg)

}