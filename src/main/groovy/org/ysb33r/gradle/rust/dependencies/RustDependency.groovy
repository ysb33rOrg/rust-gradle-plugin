//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.dependencies

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.Dependency

/** Describes a Rust dependency
 *
 * @since 0.1
 */
@CompileStatic
interface RustDependency extends Dependency {

    /** {@code toString} methods should return Cargo-correct formats.
     *
     * @return
     */
    String toString()

    /** Indicates whether the dependency is platform-specific
     *
     * @return Valid Rust target string or {@code null} is the dependency is
     * platform-independent.
     *
     */
    String getTarget()

    /** WHether the user has requested optional features for a dependency
     *
     * This not actually check whether the dependency has optinal features, it assumes the user
     * knows best.
     *
     * @return {@code true} is optional features has been requested.
     */
    boolean getHasFeatures()

    /** The list of features that the user has requested.
     *
     * @return TH elist of features requested. Can be empty, but never null.
     */
    List<String> getFeatures()
}
