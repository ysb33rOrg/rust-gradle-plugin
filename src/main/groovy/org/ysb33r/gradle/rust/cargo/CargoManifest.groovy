//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.tasks.Nested
import org.ysb33r.gradle.rust.cargo.internal.CargoPackage
import org.ysb33r.gradle.rust.dependencies.RustConfigurableDependency
import org.ysb33r.gradle.rust.dependencies.RustDependency
import org.ysb33r.gradle.rust.dependencies.RustDependencyConfiguration
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler
import org.ysb33r.gradle.rust.internal.dependencies.ConfigurableDependency
import org.ysb33r.gradle.rust.internal.dependencies.DependencyHandler

import java.util.regex.Pattern

import static groovy.lang.Closure.DELEGATE_FIRST

/** Specifies a Cargo manifest.
 *
 * @since 0.1
 */
@CompileStatic
class CargoManifest {

    CargoManifest(Project project) {
        this.project = project
        this.cargoPackage = new CargoPackage(project)
    }

    @Nested
    CargoPackage getPackage() {
        this.cargoPackage
    }

    void packageInf(Closure configure) {
        Closure configurator = (Closure) (configure.clone())
        configurator.delegate = this.cargoPackage
        configurator.resolveStrategy = DELEGATE_FIRST
        configurator()
    }

    void packageInf(Action<CargoPackage> configurator) {
        configurator.execute(this.cargoPackage)
    }

    void writeManifestTo(File manifest) {
        RustDependencyHandler rust = rustDependencies

        manifest.withWriter { Writer w ->
            writePackage(w)
            writeConfiguration(w, rust.compile)
            writeConfiguration(w, rust.test)
            writeConfiguration(w, rust.build)
            writePatches((Writer)w, rust.patchesAsMap)
        }
    }

    private void writePatches(Writer w, final Map<String, Iterable<ConfigurableDependency>> patchesMap) {
        if (!patchesMap.isEmpty()) {
            patchesMap.each { String repoName, Iterable<ConfigurableDependency> deps ->
                String cargoRepoName = repoName == DependencyHandler.CRATES_IO_NAME ? repoName : "'${repoName}'"
                w.println "[patch.${cargoRepoName}]"
                deps.each {
                    w.println it.toString()
                }
                w.println ''
            }
        }
    }

    private void writeConfiguration(Writer w, RustDependencyConfiguration configuration) {
        List<RustDependency> allDependencies = configuration.dependencies.toList()
        List<RustDependency> dependencies = allDependencies.findAll { !it.hasFeatures }
        List<RustDependency> depsWithFeatures = allDependencies.findAll { it.hasFeatures }

        Map<String, Iterable<RustDependency>> targetDependencies = configuration.targetDependencies

        if (!dependencies.empty) {
            w.println "[${configuration.name}]"

            dependencies.each {
                w.println it.toString()
            }

            w.println ''
        }

        if(!depsWithFeatures.empty) {
            depsWithFeatures.each { RustDependency it ->
                w.println "[dependency.${it.name}]"
                w.println "version = \"${it.version}\""
                w.println "default-features = ${((RustConfigurableDependency)it).withDefaultFeatures}"

                if(!it.features.empty) {
                    w.println "features = ${cargoList(it.features)}"
                }
                w.println ''
            }

        }

        if (!targetDependencies.isEmpty()) {
            targetDependencies.each { String target, Iterable<RustDependency> deps ->
                final String quote = (target ==~ TARGET_DOES_NOT_NEEDS_QUOTES) ? '' : '"'
                w.println "[target.${quote}${target}${quote}.${configuration.name}]"
                deps.each {
                    w.println it.toString()
                }
                w.println ''
            }
            w.println ""
        }
    }

    private void writePackage(Writer w) {
        w.println """[package]
name = ${cargoStr(cargoPackage.name)}
version = ${cargoStr(cargoPackage.version)}
authors = ${cargoList(cargoPackage.authors)}

"""
    }

    private String cargoStr(final String item) {
        "\"${item}\""
    }

    private String cargoList(Iterable<String> items) {
        '[' + items.collect { String item ->
            "\"${item}\""
        }.join(',') + ']'
    }

    @CompileDynamic
    private RustDependencyHandler getRustDependencies() {
        project.dependencies.rust
    }

    private Project project
    private final CargoPackage cargoPackage


    private final static Pattern TARGET_DOES_NOT_NEEDS_QUOTES = ~/(\p{Alnum}+)-(\p{Alnum}+)-(\p{Alnum}+)(-(\p{Alnum}+))?/
}
