//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecResult
import org.ysb33r.gradle.rust.RustExtension
import org.ysb33r.gradle.rust.cargo.internal.CargoCommandLineOptions
import org.ysb33r.gradle.rust.internal.RustInstaller

import static groovy.lang.Closure.DELEGATE_FIRST

/** A base class for running cargo tasks.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCargoRunnerTask extends DefaultTask {

    @Nested
    CargoOptions getOptions() {
        this.options
    }

    void options(Closure configure) {
        Closure configurator = (Closure) (configure.clone())
        configurator.delegate = this.options
        configurator.resolveStrategy = DELEGATE_FIRST
        configurator()
    }

    void options(Action<CargoOptions> configure) {
        configure.execute(this.options)
    }

    /*
Rust's package manager

Usage:
    cargo <command> [<args>...]
    cargo [options]

Options:
    -h, --help          Display this message
    -V, --version       Print version info and exit
    --list              List installed commands
    --explain CODE      Run `rustc --explain CODE`
    -v, --verbose ...   Use verbose output (-vv very verbose/build.rs output)
    -q, --quiet         No output printed to stdout
    --color WHEN        Coloring: auto, always, never
    --frozen            Require Cargo.lock and cache are up to date
    --locked            Require Cargo.lock is up to date
    -Z FLAG ...         Unstable (nightly-only) flags to Cargo

Some common cargo commands are (see all commands with --list):
    build       Compile the current project
    check       Analyze the current project and report errors, but don't build object files
    clean       Remove the target directory
    doc         Build this project's and its dependencies' documentation
    new         Create a new cargo project
    init        Create a new cargo project in an existing directory
    run         Build and execute src/main.rs
    test        Run the tests
    bench       Run the benchmarks
    update      Update dependencies listed in Cargo.lock
    search      Search registry for crates
    publish     Package and upload this project to the registry
    install     Install a Rust binary
    uninstall   Uninstall a Rust binary

See 'cargo help <command>' for more information on a specific command.

     */

    @TaskAction
    void exec() {

        List<String> args = [cargoCommand]

        args.addAll CargoCommandLineOptions.fromLogLevel(project)
        args.addAll CargoCommandLineOptions.fromOptions(getOptions())
        args.addAll cargoCommandOptions

        String exe = rustExtension.resolveCargoPath().absolutePath
        FileCollection searchPath = project.files(rustExtension.resolveCargoPath().parentFile) + rustExtension.getAbiToolsSearchPath()

        ExecResult result = runCargo(exe, searchPath.asPath, cargoWorkingDir.absolutePath, args)

        Map<String, File> outputFiles = cargoOutputFiles

        String resultText = postProcess(result.exitValue, outputFiles['output'], outputFiles['error'])

        if (result.exitValue) {

            try {
                result.assertNormalExitValue()
            } catch (Exception e) {
                throw new CargoExecutionException(
                    resultText,
                    outputFiles['output'],
                    outputFiles['error'],
                    e
                )
            }
        } else {
            logger.info resultText
        }
    }

    /** Files where Cargo output and errors are logged to.
     *
     * @return A map of two files keyed with names {@code output} and {@code error}.
     */
    Map<String, File> getCargoOutputFiles() {
        File baseDir = new File(project.buildDir, "tmp/cargo/${name}")
        [output: new File(baseDir, 'output.txt'), error: new File(baseDir, 'error.txt')]
    }

    /** Gets the Rust extension associated with this task.
     *
     * @return {@link RustExtension}.
     */
    protected RustExtension getRustExtension() {
        (RustExtension) (project.extensions.getByName(RustExtension.NAME))
    }

    /** Post processes the result for a Cargo execution and optionally return a string that can be logged
     * or used in an exception.
     *
     * @return Additional message. Default implementation is an empty string.
     */
    protected String postProcess(int exitValue, File output, File error) {
        ''
    }

    abstract protected String getCargoCommand()

    abstract protected File getCargoWorkingDir()

    abstract protected Iterable<String> getCargoCommandOptions()

    @CompileDynamic
    private ExecResult runCargo(
        final String cargoExecPath, final String searchPath, final String dir, final List<String> cargoArgs) {

        Map<String, File> outputFiles = cargoOutputFiles

        project.logger.info "Logging cargo output to ${outputFiles.output}"
        project.logger.info "Logging cargo errors to ${outputFiles.error}"

        outputFiles.output.parentFile.mkdirs()

        cargoOutputFiles.output.withOutputStream { OutputStream output ->
            cargoOutputFiles.error.withOutputStream { OutputStream error ->
                project.exec {
                    executable cargoExecPath
                    args cargoArgs
                    workingDir dir
                    environment RustInstaller.OS.pathVar, searchPath
                    ignoreExitValue true
                    standardOutput output
                    errorOutput error
                }
            }
        }
    }

    private final CargoOptions options = new CargoOptions()
    private String action
}

