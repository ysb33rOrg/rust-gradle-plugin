//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import org.ysb33r.gradle.rust.cargo.CargoOptions

/** Builds cargo command-line options.
 *
 * @since 0.1
 */
@CompileStatic
class CargoCommandLineOptions {

//    "--package"
//    "--target"
//    "--manifest-path"
//    "--no-deps"

    static List<String> fromOptions(final CargoOptions options) {
        List<String> flags = []

        if (options.frozen) {
            flags.add '--frozen'
        }

        if (options.locked) {
            flags.add '--locked'
        }

        if (options.release) {
            flags.add '--release'
        }

        flags
    }

    static List<String> fromLogLevel(final Project project) {

        if(project.logging.level == LogLevel.QUIET) {
            ['--quiet']
        } else if(project.logging.level == LogLevel.INFO) {
            ['--verbose']
        } else if(project.logging.level == LogLevel.DEBUG) {
            ['-vv']
        } else {
            []
        }
    }
}
