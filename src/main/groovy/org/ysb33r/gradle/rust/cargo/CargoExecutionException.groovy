//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo

import groovy.transform.CompileStatic

/** Thrown when execution of a {@code cargo} task fails.
 *
 * @since 0.1
 */
@CompileStatic
class CargoExecutionException extends Exception {
    CargoExecutionException(final String msg, final File output, final File error, Exception e) {
        super(formatOutput(msg, output, error), e)
    }

    private static String formatOutput(final String msg, final File output, final File error) {
        String outputMsg = ''
        String errorMsg = ''
        if(output && output.exists() && output.size() > 0) {
            outputMsg= """

Cargo output can be found at

  ${output.absoluteFile.toURI()}

"""
        }

        if(error && error.exists() && error.size() > 0) {
            errorMsg= """

Cargo error output can be found at

  ${error.absoluteFile.toURI()}

"""
        }

        "${msg}${outputMsg}${errorMsg}"
    }
}
