//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2018
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.rust.cargo.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import io.ous.jtoml.JToml
import org.gradle.api.Project
import org.ysb33r.gradle.rust.dependencies.RustDependencyConfiguration
import org.ysb33r.gradle.rust.dependencies.RustDependencyFormatException
import org.ysb33r.gradle.rust.dependencies.RustDependencyHandler
import org.ysb33r.gradle.rust.internal.dependencies.ConfigurableDependency
import org.ysb33r.gradle.rust.internal.dependencies.DependencyHandler

/** Parses a {@code Cargo.toml} file.
 *
 * @since 0.2
 */
@CompileStatic
class CargoTomlParser {

    /** Parser for a {@code Cargo.toml} file.
     *
     * @param project Project in which context the parsing will occur.
     * @param cargoPackage Cargo package to configure. Can be null in which case the {@code [package]} block will be ignored.
     * @param dependencyHandler Rust dependency handler which will be called to add dependencies.
     */
    CargoTomlParser(
        final Project project,
        final CargoPackage cargoPackage,
        final RustDependencyHandler dependencyHandler
    ) {
        this.project = project
        this.cargoPackage = cargoPackage
        this.dependencyHandler = dependencyHandler

        this.depsMap = [
            'dependencies'      : dependencyHandler.getCompile(),
            'dev-dependencies'  : dependencyHandler.getTest(),
            'build-dependencies': dependencyHandler.getBuild()
        ]
    }

    /** Parses a {@code Cargo.toml} file.
     *
     * @param cargoToml Actual file to parse.
     */
    void parseFile(final File cargoToml) {
        final Map<String, Object> context = parseToml(cargoToml)

        context.each { String section, Object items ->
            switch (section) {
                case 'package':
                    configureManifest(items)
                    break
                case 'dependencies':
                case 'dev-dependencies':
                case 'build-dependencies':
                    configureDependencies this.depsMap[section], (Map<String, Object>) items
                    break
//                case ~/^(dev-|build-)?dependencies\./:
//                    configureSingleDependency(section, items)
//                    break
                case 'target':
                    configureTargetDependencies((Map<String, Object>) items)
                    break
                case 'patch':
                    addPatches((Map<String, Object>) items)
                    break
                default:
                    project.logger.error("Does not recognise section ${section}. Ignoring.")
                    break
            }
        }
    }

    private Map<String, Object> parseToml(final File cargoToml) {
        JToml.parse(cargoToml)
    }

    @CompileDynamic
    private void configureManifest(Object o) {
        if(cargoPackage) {
            cargoPackage.name(o.name)
            cargoPackage.version(o.version)
            cargoPackage.setAuthors(o.authors as List<Object>)
        }
    }

    private void configureDependencies(
        final RustDependencyConfiguration configuration, Map<String, Object> deps, final String target = null) {
        deps.each { String name, Object value ->

            ConfigurableDependency dep = new ConfigurableDependency()
            dep.name(name)
            dep.because('Found by parsing a Cargo.toml file')

            if (target) {
                dep.target(target)
            }

            if (value instanceof CharSequence) {
                String version = value.toString()
                dep.version(version)
            } else if (value instanceof Map) {
                dep = configureSingleDependency(dep, (Map<String, Object>) value)
            } else {
                project.logger.error "Cannot parse Cargo dependency for ${name} correctly"
                dep = null
            }

            if (dep) {
                configuration.add(dep)
            }
        }
    }

    private ConfigurableDependency configureSingleDependency(
        final ConfigurableDependency dep, Map<String, Object> attributes) {
        try {
            String git
            attributes.each { String attr, Object value ->
                switch (value) {
                    case ArrayList:
                        switch (attr) {
                            case 'features':
                                dep.features((ArrayList) value)
                                break
                            default:
                                project.logger.error "Cannot parse dependency attribute '${attr}' value='${value.toString()}' correctly. Maybe a list was not expected here."
                                throw new RustDependencyFormatException('')
                        }
                        break
                    case CharSequence:
                        switch (attr) {
                            case 'version':
                                dep.version(value.toString())
                                break
                            case 'git':
                                git = value.toString()
                                dep.git(git)
                                break
                            case 'path':
                                dep.path(value.toString())
                                break
                            case 'branch':
                                dep.git(git, value.toString())
                                break
                            case 'default-features':
                                dep.withDefaultFeatures(value.toString().toBoolean())
                                break
                            default:
                                project.logger.error "Cannot parse dependency attribute '${attr}' value='${value.toString()}' correctly."
                                throw new RustDependencyFormatException('')
                        }
                        break
                    default:
                        project.logger.error "Cannot parse dependency attribute '${attr}' value='${value.toString()}' correctly."
                        throw new RustDependencyFormatException('')
                }
            }
        } catch (RustDependencyFormatException e) {
            return null
        }
        dep
    }

    private void addPatches(final Map<String, Object> patches) {
        patches.each { String repo, Object value ->
            Map<String, Object> items = (Map<String, Object>) value
            items.each { String depName, Object attributes ->
                addSinglePatch(repo, depName, (Map<String, Object>) attributes)
            }
        }
    }

    private void configureTargetDependencies(final Map<String, Object> targetPlatforms) {
        targetPlatforms.each { String target, Object value ->
            Map<String, Object> sections = (Map<String, Object>) value
            sections.each { String section, Object deps ->
                RustDependencyConfiguration configuration = depsMap[section]
                if (!configuration) {
                    project.logger.error "'${section}' is not a recoginsed Rust configuration. Will ignore."
                } else {
                    configureDependencies(configuration, (Map<String, Object>)deps, target)
                }
            }
        }
    }

    private void addSinglePatch(final String repoName, final String depName, final Map<String, Object> attributes) {
        final String path = ((Map<String, Object>) attributes)['path']?.toString()
        final String git = ((Map<String, Object>) attributes)['git']?.toString()
        final String branch = ((Map<String, Object>) attributes)['branch']?.toString()

        if (path && git) {
            project.logger.error "Found both 'path' and 'git' attributes for patch (repo=${repoName}, name=${depName}). Will ignore."
            return
        }

        if (!path && !git) {
            project.logger.error "Found neither 'path' nor 'git' attributes for patch (repo=${repoName}, name=${depName}). Will ignore."
            return
        }

        if (path) {
            if (repoName == DependencyHandler.CRATES_IO_NAME) {
                dependencyHandler.patch(depName, path)
            } else {
                dependencyHandler.patch(repoName.toURI(), depName, path)
            }
        } else {
            addSinglePatchWithGit(repoName, depName, git, branch)
        }
    }

    @CompileDynamic
    private void addSinglePatchWithGit(
        final String repoName,
        final String depName,
        final String gitRepoName,
        final String branchName
    ) {
        Closure configurator = {
            name depName
            if (branchName) {
                git gitRepoName, branchName
            } else {
                git gitRepoName
            }
        }

        if (repoName == DependencyHandler.CRATES_IO_NAME) {
            dependencyHandler.patch configurator
        } else {
            dependencyHandler.patch repoName.toURI(), configurator
        }
    }

    private void configureSingleDependency(final String groupAndName, Object o) {

    }

    private final Project project
    private final CargoPackage cargoPackage
    private final RustDependencyHandler dependencyHandler
    private final Map<String, RustDependencyConfiguration> depsMap
}
